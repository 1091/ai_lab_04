var express = require('express'),
    fs = require('fs')
    url = require('url');
var app = express();

var fileupload = require('fileupload').createFileUpload(__dirname + '/app/json/').middleware

app.post('/receive', fileupload, function(req, res) {
  // files are now in the req.body object along with other form fields
  // files also get moved to the uploadDir specified
})

app.listen(8080);
