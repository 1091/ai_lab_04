frameServices = angular.module('frameServices', [])

frameServices.factory 'Frames', ['$http', ($http) ->
  new class Frames
    constructor: ->
      @getFrames()
    
    getFrames: ()->
      $http.get('json/frame.json').then (result) ->
        result.data.frames
]
