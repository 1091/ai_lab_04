frameControllers = angular.module('frameControllers', [])


class MainController
  constructor: (@$scope, Frames) ->
    ctrlMain = @
    Frames.getFrames().then (frames) ->
      $scope.frames = frames
      ctrlMain.frames = frames
      $scope.initialFrames = angular.copy(frames)
      $scope.currentFrame = frames[0]
      $scope.showingFramesIds = []
      frame.show = true for frame in frames
      ctrlMain.addParent frames[0]
      ctrlMain.inheritSlots frames[0]
      ctrlMain.checkForSameNames frames[0]
      for sameNames in ctrlMain.namesResult
        for frameSameName in sameNames
          frameSameName.frame.ok = false
      $scope.sameNames = ctrlMain.namesResult

    @framesNames = []
    @allFrames = []
    $scope.framesNames = @framesNames
    
    @types = [
      {
        name: "bool"
        check: (input) ->
          input in @values
        values: [
          "true",
          "false"
        ],
        input: false
        ops: [
          {
            name: "True"
            f: "Function('a','return a == \"true\";')"
          },
          {
            name: "False"
            f: "Function('a','return a == \"false\";')"
          }
        ]
      },
      {
        name: "string"
        check: (input) ->
          input?
        input: true
        ops: [
          {
            name: "=="
            f: "Function('a','b', 'return a.toLowerCase() === b.toLowerCase();')"
          },
          {
            name: "!="
            f: "Function('a','b', 'return a.toLowerCase() != b.toLowerCase();')"
          }
        ]
      },
      {
        name:"int"
        check: (input) ->
          result = parseInt(input)
          return not isNaN(result)
        parseValue: (input) ->
          return parseInt input
        input: true
        ops: [
          {
            name: ">"
            f: "Function('a','b', 'return a > b;')"
          },
          {
            name: ">="
            f: "Function('a','b', 'return a >= b;')"
          },
          {
            name: "<="
            f: "Function('a','b', 'return a <= b;')"
          },
          {
            name: "=="
            f: "Function('a','b', 'return a === b;')"
          },
          {
            name: "!="
            f: "Function('a','b', 'return a != b;')"
          },
        ]
      }
    ]
        

    # $scope.newSlotType = @types[0]
    # $scope.newSlotName = "Новый слот"
    @errorsNewSlot = []

  addParent: (frame) ->
    if frame.childs? and frame.childs.length > 0
      for child in frame.childs
        child.parent = frame
        @addParent child
    else
      return true

  inheritSlots: (frame) ->
    if frame.parent?
      if frame.parent.slots?
        if not frame.slots?
          frame.slots = []
        for parentSlot in frame.parent.slots
          frameSlotOverrides = false
          for frameSlot in frame.slots
            if frameSlot.name == parentSlot.name
              frameSlotOverrides = true
          if not frameSlotOverrides
            frame.slots.push parentSlot
    if frame.childs? and frame.childs.length > 0
      for child in frame.childs
        @inheritSlots child
    else
      return true
 
  getFramesNames: (frame) ->
    if frame?
      @framesNamesArray.push frame.name
      if frame.childs?
        for child in frame.childs
          @getFramesNames child
      return @framesNamesArray

  checkFrameName: (rootFrame, frameName) ->
    # @checkForSameNames rootFrame
    if rootFrame? and frameName?
      @framesNamesArray = []
      frameNames = @getFramesNames(rootFrame)
      if frameName in frameNames
        return false
    return true

  checkForSameNames: (frame) ->
    @framesNames.push obj=
      name: frame.name
      frame: frame
    slotsNames = []
    if frame.slots?
      slotsNames.push {name: slot.name, slot: slot} for slot in frame.slots
    if frame.childs?
      for child in frame.childs
        @checkForSameNames child
    else
      result = []
      for fn in @framesNames
        repetition = _.filter(@framesNames, (elem) ->
          elem.name == fn.name
        )
        tmp = _.find(result, (elem) ->
          if repetition[0].name == elem[0].name
            true
          else
            false
        )
        if not tmp
          result.push repetition
      @namesResult = _.filter(result, (elem) ->
        elem.length >= 2
      )
      return result
  
  checkSlotName: (frame, newSlotName) ->
    if frame?
      if frame.slots?
        result = _.find(frame.slots, (elem) ->
          elem.name == newSlotName
        )
        if result?
          return false
    return true
  
  checkSlotValue: (newSlotType, newSlotValue) ->
    newSlotType.check newSlotValue


  showErrorSameNames: () ->
    if @namesResult?
      if @namesResult.length > 0
        return true
      else
        false

  haveSlots: (frame) ->
    if frame.slots?
      if frame.slots.length > 0
        return true
    false
  
  addSlot: (frame, newSlotName, newSlotType, newSlotValue) ->
    if not frame.slots?
      frame.slots = []
    if newSlotName? and newSlotType? and newSlotValue?
      if @checkSlotName(frame, newSlotName)
        if newSlotType.check?
          if @checkSlotValue(newSlotType, newSlotValue)
             obj =
              name: newSlotName
              type: newSlotType.name
            if newSlotType.parseValue?
              value = newSlotType.parseValue newSlotValue
            else
              value = newSlotValue
            obj.value = value
            frame.slots.push obj
            @inheritSlots(@frames[0])
            @$scope.newSlotName = ""
            @$scope.newSlotValue = undefined
            @$scope.newSlotType = undefined
        else
          obj =
            name: newSlotName
            type: newSlotType
            value: newSlotValue
          frame.slots.push obj
          @inheritSlots(@frames[0])

  
  deleteSlotConfirm: (frame, slot) ->
    if confirm("Вы уверены, что хотите удалить слот '" + slot.name + "'?")
      @deleteSlot frame, slot

  deleteSlot: (frame, slot) ->
    frame.slots =  _.reject(frame.slots, (elem) ->
      elem == slot
    )
    if frame.childs?
      for child in frame.childs
        @deleteSlot child, slot
    else
      return true


  
  addFrame: (parentFrame, frameName) ->
    if frameName?
      if @checkFrameName(@frames[0], frameName)
        if not parentFrame.childs?
          parentFrame.childs = []
          tmpShow = true
        else
          if parentFrame.childs[0]?
            tmpShow = parentFrame.childs[0].show
          else
            tmpShow = true
        parentFrame.childs.push obj=
          name: frameName
          parent: parentFrame
          show: tmpShow
        @$scope.newFrameName = ""
        for child in parentFrame.childs
          child.show = true
        @inheritSlots(@frames[0])

  rightFrame: (sameNamedFrames, frame) ->
    frame.ok = true
    for frameEntry in sameNamedFrames
      frameEntry.frame.parent.childs = _.reject(frameEntry.frame.parent.childs, (elem) ->
        elem == frameEntry.frame
      )
    frame.parent.childs.push frame
    # @namesResult = @namesResult.filter((elem) -> elem != sameNamedFrames)
    @namesResult = _.reject(@namesResult,
      (elem) -> elem == sameNamedFrames
    )
    while sameNamedFrames.length > 0
      sameNamedFrames.pop()
   
  removeErrorFrame: (sameNamedFrames, frameEntry) ->
    frameEntry.frame.parent.childs = _.reject(frameEntry.frame.parent.childs, (elem) ->
      elem == frameEntry.frame
    )
    @namesResult = _.reject(@namesResult, (elem) ->
      elem == sameNamedFrames
    )
    tmp = _.reject(sameNamedFrames, (elem) ->
      elem == frameEntry
    )
    if tmp.length > 0
      @namesResult.push tmp
    @$scope.sameNames = @namesResult

  frameIsNotOk: (frame) ->
    if frame.ok?
      return not frame.ok
    else
      false

  frameIsOk: (frame) ->
    if frame.ok?
      return frame.ok
    else
      true

  hideChilds: (frame) ->
    if frame.childs?
      for child in frame.childs
        child.show = false
        @hideChilds(child)
    else
      frame.show = false
      return true


  onFrameClick: (frame) ->
    if not frame.childs[0].show?
      for child in frame.childs
        child.show = true
    if frame.childs[0].show == true
      @hideChilds frame
    else
      frameChild.show = !frameChild.show for frameChild in frame.childs
     
  isFrameShowed: (frame) ->
     frame.show

  haveChilds: (frame) ->
    true if frame.childs.length > 0

  showExpand: (frame) ->
    if frame.childs?
      true if frame.childs.length > 0 and not frame.childs[0].show
    else false

  showCollapse: (frame) ->
    if frame.childs?
      true if frame.childs.length > 0 and frame.childs[0].show
    else false

  showCollapseExpand: (frame) ->
    ((@showExpand frame) or (@showCollapse frame)) and @isFrameShowed frame

  showList: (frame) ->
    frame.show

  onListClick: (frame) ->
    @$scope.currentFrame = frame

  showChilds: (frame) ->
    frameChild.show = !frameChild.show for frameChild in frame.childs
  
  showSlots: (frame) ->
    if frame?
      if frame.slots?
        true
      else
        false
    else
      false

  notCircular: (frame) ->
    if frame.childs?
      for child in frame.childs
        child.parent = undefined
        @notCircular child
    else
      return true

  serializeFrames: () ->
    if @frames?
      # _.clone()
      @notCircular(@frames[0])
      @frames

  serializeJSON: (frames) ->
    serialized = CircularJSON.stringify frames
    serialized

  getHashForString: (string) ->
    if string.length == 0
      return hash
    for chr in string
      chr   = string.charCodeAt(i)
      hash  = ((hash << 5) - hash) + chr
      hash |= 0; # Convert to 32bit integer
    
    return hash
    
  saveFrames: () ->
    @notCircular @frames[0]
    @$scope.showSave = true
    
  hideSave: () ->
    @addParent @frames[0]
    @$scope.showSave = false

  getFrames: () ->
    if @frames? and @$scope.showSave
      @frames

  loadFrames: () ->
    @$scope.showLoad = true
    
  hideLoad: () ->
    @$scope.showLoad = false

  loadNewFrames: (toLoad) ->
    if toLoad?
      @$scope.errorEmptyLoad = false
      parsed = JSON.parse(toLoad)
      @frames = parsed
      @addParent @frames[0]
      @$scope.frames = @frames
      @$scope.currentFrame = @frames[0]
      @frames[0].show = true
      @hideLoad()
      @framesNames = []
      @checkForSameNames @frames[0]
      for sameNames in @namesResult
        for frameSameName in sameNames
          frameSameName.frame.ok = false

      @$scope.sameNames = @namesResult
      @$scope.framesNames = @framesNames
    else
      @$scope.errorEmptyLoad = true

  getAllFramesIter: (frame) ->
    @allFrames.push frame
    if frame.childs?
      for child in frame.childs
        @getAllFramesIter child
    else
      return true

  getAllChilds: (frame) ->
    if frame.childs?
      for child in frame.childs
        @currentsChilds.push child
        @getAllChilds child
    else
      return true
    
  getAllFramesExceptCurrent: (frame, currentFrame) ->
    if frame?
      @allFrames = []
      @getAllFramesIter frame
      ret = _.reject(@allFrames, (elem) ->
        elem == currentFrame or elem == currentFrame.parent
      )
      @currentsChilds = []
      @getAllChilds currentFrame
      currentsChilds = @currentsChilds
      ret = _.reject(ret, (elem) ->
        for child in currentsChilds
          if elem == child
            return true
        return false
      )

  renameFrame: (frame, renamedFrameName) ->
    if renamedFrameName?
      if @checkFrameName(@frames[0], renamedFrameName)
        frame.name = renamedFrameName
        @$scope.renamedFrameName = ""
  
  changeParent: (frame, newFrameParent) ->
    if newFrameParent?
      if frame.parent.childs?
        frame.parent.childs = _.reject(frame.parent.childs, (elem) ->
          elem == frame
        )
        if not newFrameParent.childs?
          newFrameParent.childs = []
        newFrameParent.childs.push frame
        for child in newFrameParent.childs
          child.show = true
        frame.parent = newFrameParent
        @inheritSlots @frames[0]
  
  deleteFrame: (frame) ->
    if window.confirm "Вы действительно хотите удалить фрейм '" + frame.name + "'"
      frame.parent.childs = _.reject(frame.parent.childs, (elem) ->
        elem == frame
      )
      @$scope.currentFrame = frame.parent

  clearFrames: () ->
    @frames[0].childs = []
    @frames[0].name = "Корень"
    @$scope.currentFrame = @frames[0]

  renameSlot: (frame, slot) ->
    input = window.prompt "Введите новое имя для слота: ", slot.name
    if input?
      @addSlot frame, input, slot.type, slot.value
      @deleteSlot frame, slot

  startSearch: () ->
    if not @$scope.showSearch?
      @$scope.showSearch = true
    else
      @$scope.showSearch = !@$scope.showSearch

  slotsForSearch: (frame, list) ->
    if frame.slots?
      for slot in frame.slots
        if slot not in list
          inList = _.some(list, (elem) ->
            elem.name == slot.name
          )
          if not inList
            list.push slot
    if frame.childs?
      for child in frame.childs
        @slotsForSearch child, list
    else
      return true
  
  getSlotsForSearch: () ->
    if @frames?
      list = []
      @slotsForSearch @frames[0], list
      list

  getTypeByName: (typeName) ->
    ret = _.filter(@types, (elem) ->
      elem.name == typeName
    )
    ret[0]

  getPredicates: (typeName) ->
    type = @getTypeByName(typeName)
    if type?
      # @$scope.searchPredicate = type.ops[0]
      return type.ops
  
  showParentsSiblings: (frame) ->
    if frame.parent?
      frame.parent.show = true
      for child in frame.parent.childs
        child.show = true
      @showParentsSiblings frame.parent
    else
      return true

  markSearch: (frame, slot, input) ->
    if frame.slots?
      frameSlot = _.find(frame.slots, (elem) ->
        elem.name == slot.name
      )
      if frameSlot
        if @searchPredicate frameSlot.value, input
          frame.searched = true
          @showParentsSiblings frame
          console.log frameSlot, "Yes"
        else
          frame.searched = false
          console.log frameSlot, "No"
    if frame.childs?
      for child in frame.childs
        @markSearch child, slot, input
    else
      return true

  searchFunction: (searchPredicate, slot, input) ->
    @searchPredicate = eval searchPredicate.f
    console.log searchPredicate, @searchPredicate
    @markSearch @frames[0], slot, input
  
  frameSearched: (frame) ->
    frame.searched

  showSearchInput: (typeName) ->
    if typeName?
      type = @getTypeByName typeName
      type.input

  cleanSearch: (frame) ->
    frame.searched = false
    if frame.childs?
      for child in frame.childs
        @cleanSearch child
    else return true


frameControllers.controller 'MainController', [
  '$scope',
  'Frames',
  MainController
]
