(function() {
  var MainController, frameControllers,
    __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  frameControllers = angular.module('frameControllers', []);

  MainController = (function() {
    function MainController($scope, Frames) {
      var ctrlMain;
      this.$scope = $scope;
      ctrlMain = this;
      Frames.getFrames().then(function(frames) {
        var frame, frameSameName, sameNames, _i, _j, _k, _len, _len1, _len2, _ref;
        $scope.frames = frames;
        ctrlMain.frames = frames;
        $scope.initialFrames = angular.copy(frames);
        $scope.currentFrame = frames[0];
        $scope.showingFramesIds = [];
        for (_i = 0, _len = frames.length; _i < _len; _i++) {
          frame = frames[_i];
          frame.show = true;
        }
        ctrlMain.addParent(frames[0]);
        ctrlMain.inheritSlots(frames[0]);
        ctrlMain.checkForSameNames(frames[0]);
        _ref = ctrlMain.namesResult;
        for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
          sameNames = _ref[_j];
          for (_k = 0, _len2 = sameNames.length; _k < _len2; _k++) {
            frameSameName = sameNames[_k];
            frameSameName.frame.ok = false;
          }
        }
        return $scope.sameNames = ctrlMain.namesResult;
      });
      this.framesNames = [];
      this.allFrames = [];
      $scope.framesNames = this.framesNames;
      this.types = [
        {
          name: "bool",
          check: function(input) {
            return __indexOf.call(this.values, input) >= 0;
          },
          values: ["true", "false"],
          input: false,
          ops: [
            {
              name: "True",
              f: "Function('a','return a == \"true\";')"
            }, {
              name: "False",
              f: "Function('a','return a == \"false\";')"
            }
          ]
        }, {
          name: "string",
          check: function(input) {
            return input != null;
          },
          input: true,
          ops: [
            {
              name: "==",
              f: "Function('a','b', 'return a.toLowerCase() === b.toLowerCase();')"
            }, {
              name: "!=",
              f: "Function('a','b', 'return a.toLowerCase() != b.toLowerCase();')"
            }
          ]
        }, {
          name: "int",
          check: function(input) {
            var result;
            result = parseInt(input);
            return !isNaN(result);
          },
          parseValue: function(input) {
            return parseInt(input);
          },
          input: true,
          ops: [
            {
              name: ">",
              f: "Function('a','b', 'return a > b;')"
            }, {
              name: ">=",
              f: "Function('a','b', 'return a >= b;')"
            }, {
              name: "<=",
              f: "Function('a','b', 'return a <= b;')"
            }, {
              name: "==",
              f: "Function('a','b', 'return a === b;')"
            }, {
              name: "!=",
              f: "Function('a','b', 'return a != b;')"
            }
          ]
        }
      ];
      this.errorsNewSlot = [];
    }

    MainController.prototype.addParent = function(frame) {
      var child, _i, _len, _ref, _results;
      if ((frame.childs != null) && frame.childs.length > 0) {
        _ref = frame.childs;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];
          child.parent = frame;
          _results.push(this.addParent(child));
        }
        return _results;
      } else {
        return true;
      }
    };

    MainController.prototype.inheritSlots = function(frame) {
      var child, frameSlot, frameSlotOverrides, parentSlot, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2, _results;
      if (frame.parent != null) {
        if (frame.parent.slots != null) {
          if (frame.slots == null) {
            frame.slots = [];
          }
          _ref = frame.parent.slots;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            parentSlot = _ref[_i];
            frameSlotOverrides = false;
            _ref1 = frame.slots;
            for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
              frameSlot = _ref1[_j];
              if (frameSlot.name === parentSlot.name) {
                frameSlotOverrides = true;
              }
            }
            if (!frameSlotOverrides) {
              frame.slots.push(parentSlot);
            }
          }
        }
      }
      if ((frame.childs != null) && frame.childs.length > 0) {
        _ref2 = frame.childs;
        _results = [];
        for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
          child = _ref2[_k];
          _results.push(this.inheritSlots(child));
        }
        return _results;
      } else {
        return true;
      }
    };

    MainController.prototype.getFramesNames = function(frame) {
      var child, _i, _len, _ref;
      if (frame != null) {
        this.framesNamesArray.push(frame.name);
        if (frame.childs != null) {
          _ref = frame.childs;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            child = _ref[_i];
            this.getFramesNames(child);
          }
        }
        return this.framesNamesArray;
      }
    };

    MainController.prototype.checkFrameName = function(rootFrame, frameName) {
      var frameNames;
      if ((rootFrame != null) && (frameName != null)) {
        this.framesNamesArray = [];
        frameNames = this.getFramesNames(rootFrame);
        if (__indexOf.call(frameNames, frameName) >= 0) {
          return false;
        }
      }
      return true;
    };

    MainController.prototype.checkForSameNames = function(frame) {
      var child, fn, obj, repetition, result, slot, slotsNames, tmp, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2, _results;
      this.framesNames.push(obj = {
        name: frame.name,
        frame: frame
      });
      slotsNames = [];
      if (frame.slots != null) {
        _ref = frame.slots;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          slot = _ref[_i];
          slotsNames.push({
            name: slot.name,
            slot: slot
          });
        }
      }
      if (frame.childs != null) {
        _ref1 = frame.childs;
        _results = [];
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
          child = _ref1[_j];
          _results.push(this.checkForSameNames(child));
        }
        return _results;
      } else {
        result = [];
        _ref2 = this.framesNames;
        for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
          fn = _ref2[_k];
          repetition = _.filter(this.framesNames, function(elem) {
            return elem.name === fn.name;
          });
          tmp = _.find(result, function(elem) {
            if (repetition[0].name === elem[0].name) {
              return true;
            } else {
              return false;
            }
          });
          if (!tmp) {
            result.push(repetition);
          }
        }
        this.namesResult = _.filter(result, function(elem) {
          return elem.length >= 2;
        });
        return result;
      }
    };

    MainController.prototype.checkSlotName = function(frame, newSlotName) {
      var result;
      if (frame != null) {
        if (frame.slots != null) {
          result = _.find(frame.slots, function(elem) {
            return elem.name === newSlotName;
          });
          if (result != null) {
            return false;
          }
        }
      }
      return true;
    };

    MainController.prototype.checkSlotValue = function(newSlotType, newSlotValue) {
      return newSlotType.check(newSlotValue);
    };

    MainController.prototype.showErrorSameNames = function() {
      if (this.namesResult != null) {
        if (this.namesResult.length > 0) {
          return true;
        } else {
          return false;
        }
      }
    };

    MainController.prototype.haveSlots = function(frame) {
      if (frame.slots != null) {
        if (frame.slots.length > 0) {
          return true;
        }
      }
      return false;
    };

    MainController.prototype.addSlot = function(frame, newSlotName, newSlotType, newSlotValue) {
      var obj, value;
      if (frame.slots == null) {
        frame.slots = [];
      }
      if ((newSlotName != null) && (newSlotType != null) && (newSlotValue != null)) {
        if (this.checkSlotName(frame, newSlotName)) {
          if (newSlotType.check != null) {
            if (this.checkSlotValue(newSlotType, newSlotValue)) {
              obj = {
                name: newSlotName,
                type: newSlotType.name
              };
            }
            if (newSlotType.parseValue != null) {
              value = newSlotType.parseValue(newSlotValue);
            } else {
              value = newSlotValue;
            }
            obj.value = value;
            frame.slots.push(obj);
            this.inheritSlots(this.frames[0]);
            this.$scope.newSlotName = "";
            this.$scope.newSlotValue = void 0;
            return this.$scope.newSlotType = void 0;
          } else {
            obj = {
              name: newSlotName,
              type: newSlotType,
              value: newSlotValue
            };
            frame.slots.push(obj);
            return this.inheritSlots(this.frames[0]);
          }
        }
      }
    };

    MainController.prototype.deleteSlotConfirm = function(frame, slot) {
      if (confirm("Вы уверены, что хотите удалить слот '" + slot.name + "'?")) {
        return this.deleteSlot(frame, slot);
      }
    };

    MainController.prototype.deleteSlot = function(frame, slot) {
      var child, _i, _len, _ref, _results;
      frame.slots = _.reject(frame.slots, function(elem) {
        return elem === slot;
      });
      if (frame.childs != null) {
        _ref = frame.childs;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];
          _results.push(this.deleteSlot(child, slot));
        }
        return _results;
      } else {
        return true;
      }
    };

    MainController.prototype.addFrame = function(parentFrame, frameName) {
      var child, obj, tmpShow, _i, _len, _ref;
      if (frameName != null) {
        if (this.checkFrameName(this.frames[0], frameName)) {
          if (parentFrame.childs == null) {
            parentFrame.childs = [];
            tmpShow = true;
          } else {
            if (parentFrame.childs[0] != null) {
              tmpShow = parentFrame.childs[0].show;
            } else {
              tmpShow = true;
            }
          }
          parentFrame.childs.push(obj = {
            name: frameName,
            parent: parentFrame,
            show: tmpShow
          });
          this.$scope.newFrameName = "";
          _ref = parentFrame.childs;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            child = _ref[_i];
            child.show = true;
          }
          return this.inheritSlots(this.frames[0]);
        }
      }
    };

    MainController.prototype.rightFrame = function(sameNamedFrames, frame) {
      var frameEntry, _i, _len, _results;
      frame.ok = true;
      for (_i = 0, _len = sameNamedFrames.length; _i < _len; _i++) {
        frameEntry = sameNamedFrames[_i];
        frameEntry.frame.parent.childs = _.reject(frameEntry.frame.parent.childs, function(elem) {
          return elem === frameEntry.frame;
        });
      }
      frame.parent.childs.push(frame);
      this.namesResult = _.reject(this.namesResult, function(elem) {
        return elem === sameNamedFrames;
      });
      _results = [];
      while (sameNamedFrames.length > 0) {
        _results.push(sameNamedFrames.pop());
      }
      return _results;
    };

    MainController.prototype.removeErrorFrame = function(sameNamedFrames, frameEntry) {
      var tmp;
      frameEntry.frame.parent.childs = _.reject(frameEntry.frame.parent.childs, function(elem) {
        return elem === frameEntry.frame;
      });
      this.namesResult = _.reject(this.namesResult, function(elem) {
        return elem === sameNamedFrames;
      });
      tmp = _.reject(sameNamedFrames, function(elem) {
        return elem === frameEntry;
      });
      if (tmp.length > 0) {
        this.namesResult.push(tmp);
      }
      return this.$scope.sameNames = this.namesResult;
    };

    MainController.prototype.frameIsNotOk = function(frame) {
      if (frame.ok != null) {
        return !frame.ok;
      } else {
        return false;
      }
    };

    MainController.prototype.frameIsOk = function(frame) {
      if (frame.ok != null) {
        return frame.ok;
      } else {
        return true;
      }
    };

    MainController.prototype.hideChilds = function(frame) {
      var child, _i, _len, _ref, _results;
      if (frame.childs != null) {
        _ref = frame.childs;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];
          child.show = false;
          _results.push(this.hideChilds(child));
        }
        return _results;
      } else {
        frame.show = false;
        return true;
      }
    };

    MainController.prototype.onFrameClick = function(frame) {
      var child, frameChild, _i, _j, _len, _len1, _ref, _ref1, _results;
      if (frame.childs[0].show == null) {
        _ref = frame.childs;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];
          child.show = true;
        }
      }
      if (frame.childs[0].show === true) {
        return this.hideChilds(frame);
      } else {
        _ref1 = frame.childs;
        _results = [];
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
          frameChild = _ref1[_j];
          _results.push(frameChild.show = !frameChild.show);
        }
        return _results;
      }
    };

    MainController.prototype.isFrameShowed = function(frame) {
      return frame.show;
    };

    MainController.prototype.haveChilds = function(frame) {
      if (frame.childs.length > 0) {
        return true;
      }
    };

    MainController.prototype.showExpand = function(frame) {
      if (frame.childs != null) {
        if (frame.childs.length > 0 && !frame.childs[0].show) {
          return true;
        }
      } else {
        return false;
      }
    };

    MainController.prototype.showCollapse = function(frame) {
      if (frame.childs != null) {
        if (frame.childs.length > 0 && frame.childs[0].show) {
          return true;
        }
      } else {
        return false;
      }
    };

    MainController.prototype.showCollapseExpand = function(frame) {
      return ((this.showExpand(frame)) || (this.showCollapse(frame))) && this.isFrameShowed(frame);
    };

    MainController.prototype.showList = function(frame) {
      return frame.show;
    };

    MainController.prototype.onListClick = function(frame) {
      return this.$scope.currentFrame = frame;
    };

    MainController.prototype.showChilds = function(frame) {
      var frameChild, _i, _len, _ref, _results;
      _ref = frame.childs;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        frameChild = _ref[_i];
        _results.push(frameChild.show = !frameChild.show);
      }
      return _results;
    };

    MainController.prototype.showSlots = function(frame) {
      if (frame != null) {
        if (frame.slots != null) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    };

    MainController.prototype.notCircular = function(frame) {
      var child, _i, _len, _ref, _results;
      if (frame.childs != null) {
        _ref = frame.childs;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];
          child.parent = void 0;
          _results.push(this.notCircular(child));
        }
        return _results;
      } else {
        return true;
      }
    };

    MainController.prototype.serializeFrames = function() {
      if (this.frames != null) {
        this.notCircular(this.frames[0]);
        return this.frames;
      }
    };

    MainController.prototype.serializeJSON = function(frames) {
      var serialized;
      serialized = CircularJSON.stringify(frames);
      return serialized;
    };

    MainController.prototype.getHashForString = function(string) {
      var chr, hash, _i, _len;
      if (string.length === 0) {
        return hash;
      }
      for (_i = 0, _len = string.length; _i < _len; _i++) {
        chr = string[_i];
        chr = string.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0;
      }
      return hash;
    };

    MainController.prototype.saveFrames = function() {
      this.notCircular(this.frames[0]);
      return this.$scope.showSave = true;
    };

    MainController.prototype.hideSave = function() {
      this.addParent(this.frames[0]);
      return this.$scope.showSave = false;
    };

    MainController.prototype.getFrames = function() {
      if ((this.frames != null) && this.$scope.showSave) {
        return this.frames;
      }
    };

    MainController.prototype.loadFrames = function() {
      return this.$scope.showLoad = true;
    };

    MainController.prototype.hideLoad = function() {
      return this.$scope.showLoad = false;
    };

    MainController.prototype.loadNewFrames = function(toLoad) {
      var frameSameName, parsed, sameNames, _i, _j, _len, _len1, _ref;
      if (toLoad != null) {
        this.$scope.errorEmptyLoad = false;
        parsed = JSON.parse(toLoad);
        this.frames = parsed;
        this.addParent(this.frames[0]);
        this.$scope.frames = this.frames;
        this.$scope.currentFrame = this.frames[0];
        this.frames[0].show = true;
        this.hideLoad();
        this.framesNames = [];
        this.checkForSameNames(this.frames[0]);
        _ref = this.namesResult;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          sameNames = _ref[_i];
          for (_j = 0, _len1 = sameNames.length; _j < _len1; _j++) {
            frameSameName = sameNames[_j];
            frameSameName.frame.ok = false;
          }
        }
        this.$scope.sameNames = this.namesResult;
        return this.$scope.framesNames = this.framesNames;
      } else {
        return this.$scope.errorEmptyLoad = true;
      }
    };

    MainController.prototype.getAllFramesIter = function(frame) {
      var child, _i, _len, _ref, _results;
      this.allFrames.push(frame);
      if (frame.childs != null) {
        _ref = frame.childs;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];
          _results.push(this.getAllFramesIter(child));
        }
        return _results;
      } else {
        return true;
      }
    };

    MainController.prototype.getAllChilds = function(frame) {
      var child, _i, _len, _ref, _results;
      if (frame.childs != null) {
        _ref = frame.childs;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];
          this.currentsChilds.push(child);
          _results.push(this.getAllChilds(child));
        }
        return _results;
      } else {
        return true;
      }
    };

    MainController.prototype.getAllFramesExceptCurrent = function(frame, currentFrame) {
      var currentsChilds, ret;
      if (frame != null) {
        this.allFrames = [];
        this.getAllFramesIter(frame);
        ret = _.reject(this.allFrames, function(elem) {
          return elem === currentFrame || elem === currentFrame.parent;
        });
        this.currentsChilds = [];
        this.getAllChilds(currentFrame);
        currentsChilds = this.currentsChilds;
        return ret = _.reject(ret, function(elem) {
          var child, _i, _len;
          for (_i = 0, _len = currentsChilds.length; _i < _len; _i++) {
            child = currentsChilds[_i];
            if (elem === child) {
              return true;
            }
          }
          return false;
        });
      }
    };

    MainController.prototype.renameFrame = function(frame, renamedFrameName) {
      if (renamedFrameName != null) {
        if (this.checkFrameName(this.frames[0], renamedFrameName)) {
          frame.name = renamedFrameName;
          return this.$scope.renamedFrameName = "";
        }
      }
    };

    MainController.prototype.changeParent = function(frame, newFrameParent) {
      var child, _i, _len, _ref;
      if (newFrameParent != null) {
        if (frame.parent.childs != null) {
          frame.parent.childs = _.reject(frame.parent.childs, function(elem) {
            return elem === frame;
          });
          if (newFrameParent.childs == null) {
            newFrameParent.childs = [];
          }
          newFrameParent.childs.push(frame);
          _ref = newFrameParent.childs;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            child = _ref[_i];
            child.show = true;
          }
          frame.parent = newFrameParent;
          return this.inheritSlots(this.frames[0]);
        }
      }
    };

    MainController.prototype.deleteFrame = function(frame) {
      if (window.confirm("Вы действительно хотите удалить фрейм '" + frame.name + "'")) {
        frame.parent.childs = _.reject(frame.parent.childs, function(elem) {
          return elem === frame;
        });
        return this.$scope.currentFrame = frame.parent;
      }
    };

    MainController.prototype.clearFrames = function() {
      this.frames[0].childs = [];
      this.frames[0].name = "Корень";
      return this.$scope.currentFrame = this.frames[0];
    };

    MainController.prototype.renameSlot = function(frame, slot) {
      var input;
      input = window.prompt("Введите новое имя для слота: ", slot.name);
      if (input != null) {
        this.addSlot(frame, input, slot.type, slot.value);
        return this.deleteSlot(frame, slot);
      }
    };

    MainController.prototype.startSearch = function() {
      if (this.$scope.showSearch == null) {
        return this.$scope.showSearch = true;
      } else {
        return this.$scope.showSearch = !this.$scope.showSearch;
      }
    };

    MainController.prototype.slotsForSearch = function(frame, list) {
      var child, inList, slot, _i, _j, _len, _len1, _ref, _ref1, _results;
      if (frame.slots != null) {
        _ref = frame.slots;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          slot = _ref[_i];
          if (__indexOf.call(list, slot) < 0) {
            inList = _.some(list, function(elem) {
              return elem.name === slot.name;
            });
            if (!inList) {
              list.push(slot);
            }
          }
        }
      }
      if (frame.childs != null) {
        _ref1 = frame.childs;
        _results = [];
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
          child = _ref1[_j];
          _results.push(this.slotsForSearch(child, list));
        }
        return _results;
      } else {
        return true;
      }
    };

    MainController.prototype.getSlotsForSearch = function() {
      var list;
      if (this.frames != null) {
        list = [];
        this.slotsForSearch(this.frames[0], list);
        return list;
      }
    };

    MainController.prototype.getTypeByName = function(typeName) {
      var ret;
      ret = _.filter(this.types, function(elem) {
        return elem.name === typeName;
      });
      return ret[0];
    };

    MainController.prototype.getPredicates = function(typeName) {
      var type;
      type = this.getTypeByName(typeName);
      if (type != null) {
        return type.ops;
      }
    };

    MainController.prototype.showParentsSiblings = function(frame) {
      var child, _i, _len, _ref;
      if (frame.parent != null) {
        frame.parent.show = true;
        _ref = frame.parent.childs;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];
          child.show = true;
        }
        return this.showParentsSiblings(frame.parent);
      } else {
        return true;
      }
    };

    MainController.prototype.markSearch = function(frame, slot, input) {
      var child, frameSlot, _i, _len, _ref, _results;
      if (frame.slots != null) {
        frameSlot = _.find(frame.slots, function(elem) {
          return elem.name === slot.name;
        });
        if (frameSlot) {
          if (this.searchPredicate(frameSlot.value, input)) {
            frame.searched = true;
            this.showParentsSiblings(frame);
            console.log(frameSlot, "Yes");
          } else {
            frame.searched = false;
            console.log(frameSlot, "No");
          }
        }
      }
      if (frame.childs != null) {
        _ref = frame.childs;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];
          _results.push(this.markSearch(child, slot, input));
        }
        return _results;
      } else {
        return true;
      }
    };

    MainController.prototype.searchFunction = function(searchPredicate, slot, input) {
      this.searchPredicate = eval(searchPredicate.f);
      console.log(searchPredicate, this.searchPredicate);
      return this.markSearch(this.frames[0], slot, input);
    };

    MainController.prototype.frameSearched = function(frame) {
      return frame.searched;
    };

    MainController.prototype.showSearchInput = function(typeName) {
      var type;
      if (typeName != null) {
        type = this.getTypeByName(typeName);
        return type.input;
      }
    };

    MainController.prototype.cleanSearch = function(frame) {
      var child, _i, _len, _ref, _results;
      frame.searched = false;
      if (frame.childs != null) {
        _ref = frame.childs;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];
          _results.push(this.cleanSearch(child));
        }
        return _results;
      } else {
        return true;
      }
    };

    return MainController;

  })();

  frameControllers.controller('MainController', ['$scope', 'Frames', MainController]);

}).call(this);
