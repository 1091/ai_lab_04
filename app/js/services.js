(function() {
  var frameServices;

  frameServices = angular.module('frameServices', []);

  frameServices.factory('Frames', [
    '$http', function($http) {
      var Frames;
      return new (Frames = (function() {
        function Frames() {
          this.getFrames();
        }

        Frames.prototype.getFrames = function() {
          return $http.get('json/frame.json').then(function(result) {
            return result.data.frames;
          });
        };

        return Frames;

      })());
    }
  ]);

}).call(this);
